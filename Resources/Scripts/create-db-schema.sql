--
-- Create database
--

DROP DATABASE IF EXISTS `numberagency`;
CREATE DATABASE `numberagency`;
USE `numberagency`;

--
-- Table structure for table `items`
--

DROP TABLE IF EXISTS `numbers`;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `numbers` (
  `id` char(64) NOT NULL,
  `changed` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `value` BIGINT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE utf8mb4_bin;

--
-- Create user
--
GRANT SELECT, INSERT, UPDATE ON `numberagency`.`numbers` TO `numberagency`@`localhost` IDENTIFIED BY 'insecure';
FLUSH PRIVILEGES;