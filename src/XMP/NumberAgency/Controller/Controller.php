<?php
namespace XMP\NumberAgency\Controller;

use Symfony\Component\HttpFoundation\Request;

abstract class Controller implements ControllerInterface
{
    /**
     * @var Request
     */
    protected $request;

    /**
     * @var \Twig_Environment
     */
    protected $twig;


    public function __construct(Request $request, \Twig_Environment $twig)
    {
        $this->request = $request;
        $this->twig = $twig;
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public abstract function getResponse();

}