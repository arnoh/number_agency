<?php

namespace XMP\NumberAgency\Controller;


interface ControllerInterface {

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getResponse();
} 