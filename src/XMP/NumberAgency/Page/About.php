<?php

namespace XMP\NumberAgency\Page;

use Symfony\Component\HttpFoundation\Response;
use XMP\NumberAgency\Controller\Controller;

class About extends Controller
{
    /**
     * @return Response
     */
    public function getResponse()
    {
        $response = new Response();
        $template = $this->twig->load('about.twig');
        $response->setContent($template->render());
        return $response;
    }

}