<?php

namespace XMP\NumberAgency\Page;

use Symfony\Component\HttpFoundation\Response;
use XMP\NumberAgency\Controller\Controller;

class Homepage extends Controller
{
    /**
     * @return Response
     */
    public function getResponse()
    {
        $response = new Response();
        $template = $this->twig->load('homepage.twig');
        $response->setContent($template->render());
        return $response;
    }

}