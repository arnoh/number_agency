<?php
namespace XMP\NumberAgency\Page;

use Symfony\Component\HttpFoundation\Response;
use XMP\NumberAgency\Controller\Controller;
use XMP\NumberAgency\Model\NumberRepository;
use XMP\NumberAgency\Model\Number as DBNumber;



class Number extends Controller
{
    /**
     * @return Response
     */
    public function getResponse()
    {
        $response = new Response();

        $id = substr($this->request->getPathInfo(), 4);
        $id = urldecode($id);
        $length = mb_strlen($id, 'UTF-8');
        if ($length === false || $length > 64 || $length < 1) {
            $response->setStatusCode(400);
            $response->setContent('Use a more ordinary name for your number.');
            return $response;
        }

        $rp = new NumberRepository();
        $nr = $rp->getById($id);
        if ($nr === false) {
            $response->setStatusCode(500);
            $response->setContent('There seems to be a problem -- sorry.');
            return $response;
        }
        if ($nr === null) {
            $nr = new DBNumber($id);
            $rp->store($nr);
        }
        $rp->increase($nr);

        // find preferred response type
        $ctypes = $this->request->getAcceptableContentTypes();
        $ctype = 'application/json';
        foreach ($ctypes as $ct) {
            if ($ct == 'text/plain' || $ct == 'application/json') {
                $ctype = $ct;
                break;
            }
        }

        if ($ctype == 'text/plain') {
            $response->setContent($nr->value);
        } else {
            $response->setContent(json_encode(['number' => $nr->value]));
        }
        $response->headers->set('Content-Type', $ctype);
        $response->headers->set('Cache-Control', 'private, max-age=0, no-cache, no-store');
        return $response;
    }
}