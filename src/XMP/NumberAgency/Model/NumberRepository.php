<?php
namespace XMP\NumberAgency\Model;


use XMP\NumberAgency\Configuration;

class NumberRepository {

    /**
     * @var \mysqli
     */
    private $db;

    public function __construct()
    {
        $db = null;
    }

    public function store(Number $nr)
    {
        if (!$this->connectDB())
            return false;
        $stmt = $this->db->prepare('INSERT INTO numbers (id, changed, value) VALUES (?, ?, ?)');
        if (!$stmt)
            return false;
        $timestamp = $nr->changed->format('Y-m-d H:i:s');
        if (!$stmt->bind_param('sss', $nr->id, $timestamp, $nr->value))
            return false;
        if (!$stmt->execute())
            return false;
        if ($stmt->errno || $stmt->affected_rows !== 1)
            return false;

        return true;
    }

    public function increase(Number $nr)
    {
        if (!$this->connectDB())
            return false;
        $stmt = $this->db->prepare('UPDATE numbers set value=value + 1 WHERE id=?');
        if (!$stmt)
            return false;
        if (!$stmt->bind_param('s', $nr->id))
            return false;
        if (!$stmt->execute())
            return false;
        if ($stmt->errno || $stmt->affected_rows !== 1)
            return false;

        return true;
    }

    public function getById($id)
    {
        if (!$this->connectDB())
            return false;
        $stmt = $this->db->prepare('SELECT id, changed, value FROM numbers WHERE id=?');
        if (!$stmt)
            return false;
        if (!$stmt->bind_param('s', $id))
            return false;
        $nr = new Number();
        $stmt->bind_result($nr->id, $nr->changed, $nr->value);
        if (!$stmt->execute())
            return false;
        if ($stmt->errno)
            return false;
        $res = $stmt->fetch();
        if ($res === false)
            return false;
        if ($res === null)
            return null;
        return $nr;
    }

    private function connectDB()
    {
        if (!is_null($this->db))
            return true;
        $db = new \mysqli(Configuration::DB_HOST, Configuration::DB_USER, Configuration::DB_PWD, Configuration::DB_NAME);
        if (!$db || $db->connect_errno)
            return false;
        $db->set_charset('utf8mb4');
        $this->db = $db;
        return true;
    }
}