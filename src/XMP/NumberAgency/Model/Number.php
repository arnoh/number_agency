<?php
namespace XMP\NumberAgency\Model;


class Number {
    /**
     * @var string
     */
    public $id;

    /**
     * @var \DateTime
     */
    public $changed;

    /**
     * @var string
     */
    public $value;

    public function __construct($id=NULL) {
        $this->id = $id;
        $this->value = 1;
        $this->changed = new \DateTime();
    }
}