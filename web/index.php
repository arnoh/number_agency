<?php
$loader = require __DIR__.'/../vendor/autoload.php';

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use XMP\NumberAgency\Controller\Controller;
use XMP\NumberAgency\Page\Number;

$request = Request::createFromGlobals();

$map = array(
    '/' => 'XMP\NumberAgency\Page\Homepage',
    '/about' => 'XMP\NumberAgency\Page\About',
);

$twigLoader = new Twig_Loader_Filesystem(__DIR__.'/../Resources/templates');
//$twig = new Twig_Environment($twigLoader, ['cache' => __DIR__.'/../Resources/templates-compiled']);
$twig = new Twig_Environment($twigLoader);


$path = $request->getPathInfo();
if (isset($map[$path])) {
    /** @var Controller $controller */
    $controller = new $map[$path]($request, $twig);
    $response = $controller->getResponse();
}
elseif (preg_match('#^/nr/#', $path)) {
    $controller = new Number($request, $twig);
    $response = $controller->getResponse();
}
else {
    $response = new Response();
    $response->setStatusCode(404);
    $response->setContent('Not Found');
}

$response->send();
